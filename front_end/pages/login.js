import { useState, useContext } from 'react';
import { Card, Row, Col, Form, Button } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'

import UserContext from '../UserContext';
import AppHelper from '../app-helper'
import View from '../components/View'

import Head from 'next/head'

export default function index() {
    return (
        <View title={ 'Login' }>
            <Row className="justify-content-center m-0 p-0 login-container">
                <Col xs md="6" className="login-form">
                    <h3>Login</h3>
                    <LoginForm/>
                </Col>
            </Row>
        </View>
    )
}

const LoginForm = () => {
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [tokenId, setTokenId] = useState(null)

    function authenticate(e) {
        //prevent redirection via form submission
        e.preventDefault()

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            //successful authentication will return a JWT via the response accessToken property
            if(data.accessToken){
                //store JWT in local storage
                localStorage.setItem('token', data.accessToken);
                //send a fetch request to decode JWT and obtain user ID and role for storing in context
                fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    } 
                })
                .then(res => res.json())
                .then(data => {
                    //set the global user state to have properties containing authenticated user's ID
                    console.log(data)
                    setUser({
                        id: data._id
                    })
                    Router.push('/records')
                })
            }else{//authentication failure
                Router.push('/error')
            }
        })
    }

    const authenticate2 = (e) => {
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'  },
            body: JSON.stringify({ email: email, password: password })
        }
        
        fetch(`${ AppHelper.API_URL }/users/login`, options).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }
    
    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
            // console.log(data)
            setUser({ email: data.email })
            Router.push('/records')
        })
    }

    const authenticateGoogleToken = (response) => {

        setTokenId(response.tokenId)

        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${ AppHelper.API_URL }/users/verify-google-token-id`, payload).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }

    return (
        <div className="">  
            <Head>
                <title>Login</title>
            </Head>
            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label className="login-text">Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label className="login-text">Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>

                <Button variant="outline-danger" type="submit" className="login-form-button">
                    Submit
                </Button>
            </Form>
            
            <GoogleLogin onClick={ authenticate2 }
                clientId="33762832043-rhda60hvdj8ptcb51c1p6mpqt6ijsc83.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={ authenticateGoogleToken }
                onFailure={ authenticateGoogleToken }
                cookiePolicy={ 'single_host_origin' }
                className="w-100 text-center d-flex justify-content-center"
            />
        </div>
    )
}